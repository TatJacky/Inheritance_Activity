public class BookStore {
    public static void main(String[] args){
        Book[] Books = new Book[5];
        Books[0] = new Book("Tales of Zemy", "Jaxter");
        Books[1] = new ElectronicBook("Like a somebody", "Buk Lao", 24.2);
        Books[2] = new Book("Cooking with Zaky", "Godon");
        Books[3] = new ElectronicBook("hehexd", "tyler1", 420);
        Books[4] = new ElectronicBook("LATAAA B****", "Troc3j", 4.20);

        for(Book x: Books){
            System.out.println(x);
        }
    }
}
