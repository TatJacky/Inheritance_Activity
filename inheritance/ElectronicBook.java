public class ElectronicBook extends Book{
    private double numberBytes;

    public ElectronicBook(String title, String author, double numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public double getNumberBytes(){
        return this.numberBytes;
    }

    @Override
    public String toString(){
        return super.toString()+"\n"
            +"File Size: "+this.numberBytes;
    }
}
